# Vault Provisioning

Terraform and provisioning scripts for using ssh with vault certificate signing.

This setup is for multiple machines that have hostnames like `*.<your-domain-name>`. It could also work for a single host, but it's more useful for more hosts. The author uses CloudFlare to maintain these DNS records which point at private IP addresses.

The author uses this setup to provision ssh access for a HTPC and many raspberry-pi machines.

## SSH Keys

This document and project require you use an `ed25519` key-pair. This is the most secure key-pair type, so it's encouraged that you use it.

## Vault

You'll need a vault server/cluster somewhere. Installing, running and maintaining one is outside of the scope of this document.

## Instructions

### Terraform

You'll need a private and public key from a CA. You can generate these yourself with something like `ssh-keygen -o -a 100 -t ed25519 -f ~/.ssh/id_ed25519_vault -C you@example.com` if you don't have one. Put these into `terraform/secret.auto.tfvars` along with a `username` variable which is the default ssh user and an `allowed_domains` variable which is the base name for your domain (the part after the `*`).

Ensure you have a `VAULT_ADDR` environment variable in your shell pointing to your vault server.

Apply the terraform with `terraform apply` in the `terraform` directory to setup the vault ssh backends for hosts and clients.

### SSH Hosts

Login to your ssh hosts, copy the `provision-ssh-host` script,  install `vault`, and obtain a vault token with `vault login ...`.

Execute `./provision-ssh-host`. It will ask for root password for `sudo` to install the new ssh keys and client public key.

Put the following at the end of `/etc/ssh/sshd_config`:

```
# Vault Trusted CA
TrustedUserCAKeys /etc/ssh/trusted-user-ca-keys.pem
HostKey /etc/ssh/ssh_host_ed25519_key
HostCertificate /etc/ssh/ssh_host_ed25519_key-cert.pub
```

Restart your ssh server with `sudo systemctl restart sshd` to apply the new configuration.

### SSH Clients

Run `echo "@cert-authority *.<your-hostname-base> $(vault read -field=public_key ssh-host-signer/config/ca)" >> ~/.ssh/known_hosts`

Use `vault-ssh` to ssh to your host, like `vault-ssh host.<your-domain-name>`. You should be able to login without any password and without needing to trust any new hosts (the above command provides the trust for all your machines).

This script and configuration uses a single username, which is `${USER}` so you'll need the same username as your client machine on all the host machines.

## How to improve

You could probably provision the host machines with something like Chef or Ansible to configure the ssh server.
