resource "vault_mount" "ssh-client-signer" {
  type = "ssh"
  path = "ssh-client-signer"
}

resource "vault_ssh_secret_backend_ca" "ssh-client-signer" {
  backend = vault_mount.ssh-client-signer.path

  generate_signing_key = false
  public_key           = var.public_key
  private_key          = var.private_key
}

resource "vault_ssh_secret_backend_role" "client" {
  name                    = "client"
  backend                 = vault_mount.ssh-client-signer.path
  key_type                = "ca"
  allowed_users           = "*"
  default_user            = var.username
  allow_user_certificates = true
  ttl                     = "45"
  default_extensions = {
    "permit-pty" : ""
  }
}
